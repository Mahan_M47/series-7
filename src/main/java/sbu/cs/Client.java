package sbu.cs;

import java.io.*;
import java.net.Socket;

public class Client {

    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;
    public static final int BYTE_ARRAY_SIZE = (int) Math.pow(2, 15);   //the file will be sent as 2^15 byte packets.

    public Client (String ADDRESS, int PORT) throws IOException
    {
        System.out.println("Connecting...");
        boolean flag = true;

        while (flag)
        {
            try {
                socket = new Socket(ADDRESS, PORT);
                flag = false;
            }
            catch (Exception e)
            {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
            }
        }

        System.out.println("Connection Established.");
        out = new DataOutputStream( socket.getOutputStream() );
    }

    public void readFile(String filePath) throws IOException
    {
        // send the filename
        out.writeUTF(filePath);

        // read the file as bytes
        File file = new File(filePath);
        in = new DataInputStream( new FileInputStream(file) );
        byte[] bytes = new byte[BYTE_ARRAY_SIZE];

        System.out.println("Sending File...");

        int count;
        while ((count = in.read(bytes)) > 0) {
            out.write(bytes, 0, count);
        }

        System.out.println("File was Successfully Sent.");
    }

    public void shutDown()
    {
        try {
            in.close();
            out.close();
            socket.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create a socket and connect to server then send fileName and after that send file to be saved on server side.
     *  in here filePath == fileName
     *
     * @param args a string array with one element
     * @throws IOException
     */
    public static void main(String[] args) throws IOException
    {
        String filePath = args[0];      // "sbu.png" or "book.pdf"

        Client client = new Client("localhost", 6000);
        client.readFile(filePath);
        client.shutDown();
    }
}
