package sbu.cs;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Server {

    private final ServerSocket serverSocket;
    private final Socket socket;
    private DataInputStream in;
    private DataOutputStream out;

    public Server (int PORT) throws IOException
    {
        serverSocket = new ServerSocket(PORT);

        System.out.println("Waiting for client...");
        socket = serverSocket.accept();
        System.out.println("Connection Established.");

        in = new DataInputStream( socket.getInputStream() );
    }

    public void writeFile(String directory) throws IOException
    {
        // receive file name
        String fileName = in.readUTF();

        // create the directory
        Files.createDirectories( Paths.get(directory) );

        // receive and write the file from socket
        File file = new File(directory + "/" + fileName);
        out = new DataOutputStream( new FileOutputStream(file) );
        byte[] bytes = new byte[Client.BYTE_ARRAY_SIZE];

        System.out.println("Receiving File...");

        int count;
        while ( (count = in.read(bytes)) > 0) {
            out.write(bytes,0, count);
        }

        System.out.println("File Has Been Copied to " + directory + "/" + fileName);
    }

    public void shutDown()
    {
        try {
            in.close();
            out.close();
            socket.close();
            serverSocket.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create a server here and wait for connection to establish,
     *  then get file name and after that get file and save it in the given directory
     *
     * @param args an string array with one element
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        // below is the name of directory which you must save the file in it
        String directory = args[0];     // default: "server-database"

        Server server = new Server(6000);
        server.writeFile(directory);
        server.shutDown();
    }
}
